package com.example.demo.helpingclass;

import java.util.Collections;
import java.util.Comparator;  
import java.util.LinkedHashMap;  
import java.util.LinkedList;  
import java.util.List;  
import java.util.Map;  
import java.util.Map.Entry;  
  
public class Demo  
{  
	Map<String, Long> map;
public Demo(Map<String, Long> map) {
	this.map=map;
	
}
public void sortByValue(boolean order)   
{  
//convert HashMap into List   
List<Entry<String, Long>> list = new LinkedList<Entry<String, Long>>(map.entrySet());  
//sorting the list elements  
Collections.sort(list, new Comparator<Entry<String, Long>>()   
{  
public int compare(Entry<String, Long> o1, Entry<String, Long> o2)   
{  
if (order)   
{  
//compare two object and return an integer  
return o1.getValue().compareTo(o2.getValue());}   
else   
{  
return o2.getValue().compareTo(o1.getValue());  
}  
}  
});  
//prints the sorted HashMap  
 
}
}  