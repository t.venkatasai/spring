package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Orders;
import com.example.demo.services.OrdersService;

@RestController
@ResponseBody
@CrossOrigin(origins="*")
public class HomeController {

	private OrdersService ordersService;
	
	@Autowired
	public HomeController(OrdersService ordersService) {
		this.ordersService=ordersService;
	}
	
	@GetMapping("/status")
	public List<JSONObject> getAllOrders() {
		return ordersService.getAllOrdersList();
	}
	
	@GetMapping("/{cityid}")
	public List<Orders> getOrdersInCity(@PathVariable (value="cityid") Long city_id){
//		Long count = ordersService.getOrdersInCityList(city);
//		HashMap<Long,Long> h = new HashMap<Long, Long>();
//		h.put(city_id, count);
		return ordersService.getOrdersInCityList(city_id);
	}
	@GetMapping("/api/{cityid}")
	public HashMap<Long,Long>  getCountOrdersInCity(@PathVariable (value="cityid") Long city_id){
		Long count = ordersService.getCountOrdersInCity(city_id);
		HashMap<Long,Long> h = new HashMap<Long, Long>();
		h.put(city_id, count);
		return h;
	}
	
	@GetMapping("/geography")
	public HashMap<String,Long> getCityIdAndCityName(){
		HashMap<String,Long> totalCitiesWithCount = new HashMap<String, Long>();
		List noOfCities =  ordersService.getCityIdAndCityName();
		for (int i = 0; i < noOfCities.size(); i++) {
			Long count = ordersService.getCountOrdersInCity((long) (i+1));
			totalCitiesWithCount.put((String) noOfCities.get(i), count);
		}
		return totalCitiesWithCount; 
	}
	
	@GetMapping("/api/citiesWithLessOrders")
	public List<JSONObject> getcitiesWithLessOrders() {
		return ordersService.getCitiesWithLessOrders();
	}
	
	@GetMapping("/api/citiesWithMoreOrders")
	public List<JSONObject> getcitiesWithMoreOrders() {
		return ordersService.getCitiesWithMoreOrders();
	}
	
	@GetMapping("/trends/itemsInTrending")
	public List<JSONObject> getTrendingItems() {
		return ordersService.getItemsWhichAreTrending();
	}
	
	@GetMapping("/trends/itemsInTrendingInCities")
	public List<JSONObject> getTrendingItemsCities() {
		return ordersService.getItemsWhichAreTrendingInCities();
	}
	
	@GetMapping("/trends/{city}")
	public List<JSONObject> getParticularCityTrendingItem(@PathVariable (value="city") String city){
		return ordersService.getItemsWhichAreTrendingInParticularCities(city);
	}
}
