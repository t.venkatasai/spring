package com.example.demo.repositories;

import java.util.HashMap;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Geography;

public interface GeographyRepository extends JpaRepository<Geography, Long>{
	
	@Query(value = "SELECT city FROM geography", nativeQuery = true)
	List findAllByCity();

}
